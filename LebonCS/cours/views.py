from django.shortcuts import render,redirect,HttpResponse
from .forms import CoursForm
from utilisateur.models import Profil
from .models import Cours
from covoiturage.models import Covoiturage
from objets.models import Objets
from evenements.models import Evenements
from django.contrib.auth.decorators import login_required

@login_required
def formulaire(request):

    form = CoursForm(request.POST or None) #on vérifie que le formulaire est rempli, sinon on en envoie un vide
    if form.is_valid(): 
        cours = form.save(commit=False) #commit de nom et description
        cours.utilisateur_id = Profil.objects.get(user__id=request.user.id).id #lien ForeignKey avec Profil
        cours.save() #enregistrement d'une entrée (nom,description,utilisateur) dans la table Cours
        return redirect('/cours/')

    return render(request, 'cours/formulaire_cours.html', locals())

@login_required
def modifier(request,profil_id,id_cours):
    cours=Cours.objects.get(id=id_cours)  # on récupère le cours que l'utilisateur veut modifier
    Cours.objects.filter(id=id_cours).delete()# On le supprime des annonces
    form = CoursForm(instance=cours)# on renvoie le formulaire prêt à être modifié

    return render(request, 'cours/formulaire_cours.html', locals())

def actu(request) :
    cours = Cours.objects.order_by('-date_post').all() # On affiche les annonces de tous les utilisateurs
    return render(request,'cours/actu_cours.html',locals())

@login_required
def supprimer(request,profil_id,id_cours):
    Cours.objects.filter(id=id_cours).delete()
    objets=Objets.objects.filter(utilisateur__id=profil_id)
    evenements=Evenements.objects.filter(utilisateur__id=profil_id)
    cours=Cours.objects.filter(utilisateur__id=profil_id)
    covoiturages=Covoiturage.objects.filter(utilisateur__id=profil_id)

    return render(request,'utilisateur/page_perso.html',locals())

def search(request) :
    #récupération de la recherche
    nom=request.GET.get('searchForm',None)

    #récupération objets correspondant à la recherche (des queryset)

    nomCorrespondant=set(Cours.objects.filter(nom__icontains=nom))
    descrCorrespondnat=set(Cours.objects.filter(description__icontains=nom))
    CoursCorrespondant= list(nomCorrespondant.union(descrCorrespondnat))
    return render(request,'cours/searchcours.html',{'resultat':CoursCorrespondant})