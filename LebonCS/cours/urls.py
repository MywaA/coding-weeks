from django.urls import re_path, path
from django.conf.urls import url
from .views import formulaire,actu,supprimer,modifier,search

urlpatterns = [
    path('formulaire/', formulaire, name='formulaire_cours'),
    path('', actu, name='actu_cours'),
    path('suppr_cours/<int:profil_id>/<int:id_cours>', supprimer , name='supprimer_cours'),
    path('modif_cours/<int:profil_id>/<int:id_cours>', modifier , name='modifier_cours'),
    path('search/',search,name='searchcours')
]