from django.test import TestCase, Client
from django.urls import resolve
from cours.views import formulaire, actu
from django.contrib.auth import login
from django.contrib.auth.models import User


class Cours_page_test(TestCase):
    def test_actu_cours(self):              #Test d'url pour l'onglet Cours
        found = resolve('/cours/')
        self.assertEqual(found.func, actu) 
    
    def test_cours_formulaire(self):            #Test d'url our l'onglet Formulaire de la catégorie Cours
        found = resolve('/cours/formulaire/')
        self.assertEqual(found.func, formulaire)

    def test_cours_contenu(self):           #Test de contenu
        response=self.client.get('/cours/')
        message="Cours"
        self.assertContains(response, message)