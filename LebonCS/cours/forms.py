from django import forms
from .models import Cours

#Formulaire pour mettre en ligne un cours à échanger

class CoursForm(forms.ModelForm):
    class Meta:
        model = Cours
        fields = ('nom','description','souhait')
        widgets = {'description': forms.Textarea}
        labels = {'nom':'Intitulé du cours','description':'Description (facultatif)','souhait':"Contre quoi aimerais-tu l'échanger ? (facultatif)"}