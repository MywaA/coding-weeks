from django.db import models
from utilisateur.models import Profil
import django.utils

# Create your models here.

class Cours(models.Model):
    nom = models.CharField(max_length=200)
    description = models.CharField(max_length=5000,null=True,blank=True)
    souhait = models.CharField(max_length=200,null=True,blank=True) #Cours demandé en échange
    date_post=models.DateTimeField(default=django.utils.timezone.now)
    utilisateur = models.ForeignKey(Profil, on_delete=models.CASCADE)
    
    def __str__(self):
        return self.nom