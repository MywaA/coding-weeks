from django.db import models
from utilisateur.models import Profil
from django.core.validators import MinValueValidator

#Création du modèle covoiturage

class Covoiturage(models.Model):
    lieu_depart = models.CharField(max_length=200)
    lieu_arrivee = models.CharField(max_length=200)
    date = models.DateTimeField()  #date de départ
    heure = models.CharField(null=True,blank=True,max_length=30)    #heure de départ
    nombre_places = models.PositiveIntegerField() #nombre de places disponibles
    specifications = models.CharField(max_length=5000,null=True,blank=True) #renseignements complémentaires apportés par le conducteur
    prix = models.DecimalField(max_digits=10, decimal_places=2,null=True,blank=True,validators=[MinValueValidator(0)])
    utilisateur = models.ForeignKey(Profil, on_delete=models.CASCADE)

    def __str__(self):
        return "Covoiturage de {} à {}".format(self.lieu_depart, self.lieu_arrivee)