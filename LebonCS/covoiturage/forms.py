from django import forms
from .models import Covoiturage

#Formulaire pour mettre en ligne un covoiturage.

class CovoiturageForm(forms.ModelForm):
    class Meta:
        model = Covoiturage
        fields = ('lieu_depart','lieu_arrivee','date','heure','nombre_places','specifications','prix')
        widgets = {'specifications': forms.Textarea,'date': forms.SelectDateWidget}
        labels = {'lieu_depart':"Lieu de départ",'lieu_arrivee':"Lieu d'arrivée",'date':"Date de départ",'heure':'Heure approximative de départ (facultatif)','nombre_places':'Nombre de places disponibles','specifications':"Spécifications (non fumeur, taille du véhicule pour les bagages, etc) (facultatif)",'prix':"Prix approximatif d'une place (facultatif)"}