from django.urls import re_path, path
from django.conf.urls import url
from .views import formulaire,actu,supprimer,modifier,search

urlpatterns = [
    path('formulaire/', formulaire, name='formulaire_covoiturage'),
    path('', actu, name='actu_covoiturage'),
    path('suppr_covoiturage/<int:profil_id>/<int:id_covoiturage>', supprimer , name='supprimer_covoiturage'),
    path('modif_covoiturage/<int:profil_id>/<int:id_covoiturage>', modifier , name='modifier_covoiturage'),
    path('search/',search,name='searchcovoit')
]