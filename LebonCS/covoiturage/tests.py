from django.test import TestCase, Client
from django.urls import resolve
from covoiturage.views import formulaire, actu
from django.contrib.auth import login
from django.contrib.auth.models import User


class Covoiturage_page_test(TestCase):
    def test_actu_covoiturage(self):        #Test d'url
        found = resolve('/covoiturage/')
        self.assertEqual(found.func, actu) 
    
    def test_covoiturage_formulaire(self):      #Test d'url
        found = resolve('/covoiturage/formulaire/')
        self.assertEqual(found.func, formulaire)
    
    def test_covoiturage_contenu(self):     #Test de contenu
        response=self.client.get('/covoiturage/')
        message="Covoiturage"
        self.assertContains(response, message)