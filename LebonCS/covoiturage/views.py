from django.shortcuts import render,redirect,HttpResponse
from .forms import CovoiturageForm
from utilisateur.models import Profil
from .models import Covoiturage
from cours.models import Cours
from objets.models import Objets
from evenements.models import Evenements
from django.contrib.auth.decorators import login_required
from datetime import date

@login_required
def formulaire(request):
    
    form = CovoiturageForm(request.POST or None) #on vérifie que le formulaire est rempli, sinon on en envoie un vide
    if form.is_valid(): 
        covoiturage = form.save(commit=False) #commit de lieu_depart,lieu_arrivee,date,heure,nombre_places,specifications,prix
        covoiturage.utilisateur_id = Profil.objects.get(user__id=request.user.id).id #lien ForeignKey avec Profil
        covoiturage.save() #enregistrement d'une entrée (lieu_depart,lieu_arrivee,date,heure,nombre_places,specifications,prix,utilisateur) dans la table Covoiturage
        return redirect('/covoiturage/')

    return render(request, 'covoiturage/formulaire_covoiturage.html', locals())

@login_required
def modifier(request,profil_id,id_covoiturage):
    covoiturage=Covoiturage.objects.get(id=id_covoiturage) # on récupère le covoiturage que l'utilisateur veut modifier
    Covoiturage.objects.filter(id=id_covoiturage).delete()# On le supprime des annonces
    form = CovoiturageForm(instance=covoiturage)# on renvoie le formulaire prêt à être modifié
    return render(request, 'covoiturage/formulaire_covoiturage.html', locals())

def actu(request) :
    covoiturages = Covoiturage.objects.order_by('date').filter(date__gte=date.today()) # On affiche les annonces de tous les utilisateurs
    return render(request,'covoiturage/actu_covoiturage.html',locals())

@login_required
def supprimer(request,profil_id,id_covoiturage):
    Covoiturage.objects.filter(id=id_covoiturage).delete()
    objets=Objets.objects.filter(utilisateur__id=profil_id)
    evenements=Evenements.objects.filter(utilisateur__id=profil_id)
    cours=Cours.objects.filter(utilisateur__id=profil_id)
    covoiturages=Covoiturage.objects.filter(utilisateur__id=profil_id)

    return render(request,'utilisateur/page_perso.html',locals())

def search(request) :
    #récupération de la recherche
    nom=request.GET.get('searchForm1',None)

    #récupération objets correspondant à la recherche (des queryset)

    departCorrespondant=set(Covoiturage.objects.filter(lieu_depart__icontains=nom))
    arriveCorrespondant=set(Covoiturage.objects.filter(lieu_arrivee__icontains=nom))
    CovoitCorrespondant= list(departCorrespondant.union(arriveCorrespondant))
    return render(request,'covoiturage/covoituragesearch.html',{'resultat':CovoitCorrespondant})