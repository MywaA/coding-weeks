# Generated by Django 2.2.7 on 2019-11-21 10:46

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('covoiturage', '0006_auto_20191121_1122'),
    ]

    operations = [
        migrations.AlterField(
            model_name='covoiturage',
            name='prix',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=10, null=True, validators=[django.core.validators.MinValueValidator(0.01)]),
        ),
    ]
