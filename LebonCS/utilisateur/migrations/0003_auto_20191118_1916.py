# Generated by Django 2.2.7 on 2019-11-18 18:16

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('utilisateur', '0002_auto_20191118_1557'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profil',
            name='mail',
            field=models.EmailField(default='mail', max_length=254),
            preserve_default=False,
        ),
    ]
