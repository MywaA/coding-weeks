# Generated by Django 2.2.7 on 2019-11-18 19:58

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('utilisateur', '0003_auto_20191118_1916'),
    ]

    operations = [
        migrations.AddField(
            model_name='profil',
            name='nom',
            field=models.CharField(default='nom', max_length=30),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='profil',
            name='prenom',
            field=models.CharField(default='prenom', max_length=30),
            preserve_default=False,
        ),
    ]
