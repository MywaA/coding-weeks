from django import forms
from .models import Profil,Support

class ConnexionForm(forms.Form):
    username = forms.CharField(label="Nom d'utilisateur", max_length=30)
    password = forms.CharField(label="Mot de passe", widget=forms.PasswordInput)

class RegisterForm(forms.ModelForm):
    class Meta():
         model = Profil
         fields = ('prenom','nom','annee','mail','tel','pdp')
         labels = {'prenom':'Prénom','nom':'Nom','annee':'En quelle année êtes-vous ?','mail':'Adresse Mail','tel':'Numéro de Téléphone (facultatif)','pdp':'Photo de profil (facultatif)'}

class SupportForm(forms.ModelForm):
    class Meta():
        model = Support
        fields = ('nom','mail','tel','msg')
        widgets = {'msg': forms.Textarea}
        labels = {'nom':'Nom/Prénom','mail':'Mail','tel':'Numéro de Téléphone','msg':'Message'}