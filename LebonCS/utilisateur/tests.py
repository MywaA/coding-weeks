from django.test import TestCase, Client
from django.urls import resolve
from utilisateur.views import registration,connexion,deconnexion,support
from django.contrib.auth import login
from django.contrib.auth.models import User

class Connexion_page_test(TestCase):
    def test_root_url_resolves_to_welcome_page_view(self):
        found = resolve('/')
        self.assertEqual(found.func, support)

    def test_register_url_resolves_to_register_view(self):
        found = resolve('/register/')
        self.assertEqual(found.func, registration)       

    def test_connexion_url_resolves_to_connexion_view(self):
        found = resolve('/connexion/')
        self.assertEqual(found.func, connexion)       

    def test_deconnexion_url_resolves_to_deconnexion_view(self):
        found = resolve('/deconnexion/')
        self.assertEqual(found.func, deconnexion)    

    def test_index_page(self):
        response=self.client.get('')
        message="Accueil"
        self.assertContains(response, message)

    def test_register_page(self):
        response=self.client.get('/register/')
        message="S'enregistrer"
        self.assertContains(response, message)

    def test_connexion_page(self):
        response=self.client.get('/connexion/')
        message="Se connecter"
        self.assertContains(response, message)

    def test_connexion_simulation_client(self):
        user = User.objects.create(username = 'test_user')
        user.set_password('12345678')
        user.save()
        c = Client()
        c.login(username = 'test_user', password ='12345678')
        response = c.get('/connexion/')
        message="Vous êtes connecté"
        self.assertContains(response, message)