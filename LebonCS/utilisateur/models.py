from django.db import models
from django.contrib.auth.models import User

class Profil(models.Model):
    user = models.OneToOneField(User,on_delete=models.CASCADE)  #liaison OneToOne vers le modèle User
    pdp = models.ImageField(null=True, blank=True, upload_to="pdp/")
    annee = models.CharField(choices=[('1A', '1A'),('2A', '2A'),('3A', '3A'),],max_length=30)
    mail = models.EmailField()
    tel = models.CharField(blank=True,null=True,max_length=10)
    prenom = models.CharField(max_length=30)
    nom = models.CharField(max_length=30)

    def __str__(self):
        return "Profil de {0}".format(self.user.username)

class Support(models.Model):
    nom = models.CharField(max_length=30)
    mail = models.EmailField()
    tel = models.CharField(max_length=10)
    msg = models.CharField(max_length=10000)

    def __str__(self):
        return "Message de {0}".format(self.nom)