from django.shortcuts import render,redirect
from utilisateur.forms import RegisterForm,ConnexionForm,SupportForm
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login, logout
from .models import Profil
from django.contrib.auth.decorators import login_required
from cours.models import Cours
from covoiturage.models import Covoiturage
from objets.models import Objets
from evenements.models import Evenements

def registration(request):
    logout(request) #si l'utilisateur est connecté mais parvient à aller sur la page d'enregistrement, alors il est automatiquement déconnecté
    
    if request.method == 'POST': #on vérifie que les formulaires sont remplis, sinon on en envoie des vides 
        #récupération des données des formulaires entrées par l'utilisateur
        user_form = UserCreationForm(request.POST) #username/mdp/confirmation du mdp
        profile_form = RegisterForm(request.POST,request.FILES) #prenom/nom/annee/mail/tel/pdp
        if user_form.is_valid() and profile_form.is_valid():
            user = user_form.save() #enregistrement d'une entrée (username,mdp) dans la table User
            profile = profile_form.save(commit=False) #commit de mail et année
            profile.user_id = user.id #lien OneToOne entre Profil et User           
            profile.save() #enregistrement d'une entrée (prenom,nom,annee,mail,tel (si précisé),pdp (si précisé),user) dans la table Profil
            return redirect(connexion) #l'enregistrement est réussi, redirection vers la page de connexion

    else : #envoi des formulaires d'enregistrement vides
        user_form = UserCreationForm()
        profile_form = RegisterForm()

    return render(request,'utilisateur/register.html',locals())
            
def connexion(request):
    error = False
    if request.method == "POST": #on vérifie que le formulaire est rempli, sinon on en envoie un vide
        form = ConnexionForm(request.POST) #récupération des données du formulaire entrées par l'utilisateur
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            user = authenticate(username=username, password=password)  #on vérifie si les username et mot de passe entrés sont correctes
            if user:  #si c'est le cas, on connecte l'utilisateur
                login(request, user)
                return redirect('accueil')
            else: #sinon, une erreur sera affichée
                error = True
    else:
        form = ConnexionForm() #envoi d'un formulaire de connexion vide

    return render(request, 'utilisateur/connexion.html', locals())

def deconnexion(request):
    #on déconnecte l'utilisateur et le redirige vers la page de connexion
    logout(request)
    return redirect(connexion)

def page_contact(request,id):
    vendeur=Profil.objects.get(id=id)
    return render (request,'utilisateur/page_contact.html',locals())

@login_required
def page_perso(request) :
    profil_id = Profil.objects.get(user__id=request.user.id).id
    objets=Objets.objects.filter(utilisateur__id=profil_id)
    evenements=Evenements.objects.filter(utilisateur__id=profil_id)
    cours=Cours.objects.filter(utilisateur__id=profil_id)
    covoiturages=Covoiturage.objects.filter(utilisateur__id=profil_id)

    return render(request,'utilisateur/page_perso.html',locals())

def support(request):
    form = SupportForm(request.POST or None) 
    if form.is_valid() :
        support = form.save()
        form = SupportForm() 
    return render(request,'utilisateur/index.html',locals())