from django.shortcuts import render,redirect,HttpResponse
from .forms import EvenementsForm
from utilisateur.models import Profil
from .models import Evenements
from cours.models import Cours
from covoiturage.models import Covoiturage
from objets.models import Objets
from django.contrib.auth.decorators import login_required
from datetime import date

@login_required
def formulaire(request):
    
    form = EvenementsForm(request.POST or None) #on vérifie que le formulaire est rempli, sinon on en envoie un vide
    if form.is_valid(): 
        evenement = form.save(commit=False) #commit de nom,date,description,prix,don
        evenement.utilisateur_id = Profil.objects.get(user__id=request.user.id).id #lien ForeignKey avec Profil
        evenement.save() #enregistrement d'une entrée (nom,description,photo,prix,don,utilisateur) dans la table Evènement
        return redirect('/evenements/')

    return render(request, 'evenements/formulaire_evenements.html', locals())

@login_required
def modifier(request,profil_id,id_evenement):
    evenement=Evenements.objects.get(id=id_evenement) #On récupère l'évênement que l'utilisateur veut modifier
    Evenements.objects.filter(id=id_evenement).delete()# On le supprime des annonces
    form = EvenementsForm(instance=evenement)# on renvoie le formulaire prêt à être modifié
    return render(request, 'evenements/formulaire_evenements.html', locals())

def actu(request) :
    evenements = Evenements.objects.order_by('date_debut').filter(date_debut__gte=date.today()) # On affiche les annonces de tous les utilisateurs
    return render(request,'evenements/actu_evenements.html',locals())

@login_required
def supprimer(request,profil_id,id_evenement):
    # On affiche les annonces de l'utilisateur qui peut les supprimer
    Evenements.objects.filter(id=id_evenement).delete()
    objets=Objets.objects.filter(utilisateur__id=profil_id)
    evenements=Evenements.objects.filter(utilisateur__id=profil_id)
    cours=Cours.objects.filter(utilisateur__id=profil_id)
    covoiturages=Covoiturage.objects.filter(utilisateur__id=profil_id)

    return render(request,'utilisateur/page_perso.html',locals())

def search(request) :
    #récupération de la recherche
    nom=request.GET.get('searchForm2',None)

    #récupération objets correspondant à la recherche (des queryset)

    nomCorrespondant=set(Evenements.objects.filter(nom__icontains=nom))
    descrCorrespondant=set(Evenements.objects.filter(description__icontains=nom))
    eventsCorrespondant= list(nomCorrespondant.union(descrCorrespondant))
    return render(request,'evenements/eventsearch.html',{'resultat':eventsCorrespondant})  