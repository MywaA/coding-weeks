from django.db import models
from utilisateur.models import Profil
from django.core.validators import MinValueValidator
from datetime import date

#Création du modèle Evenements

class Evenements(models.Model):
    nom = models.CharField(max_length=200)
    description = models.CharField(max_length=5000,null=True,blank=True)
    date_debut = models.DateTimeField()
    heure_debut = models.CharField(null=True,blank=True,max_length=30)
    date_fin = models.DateTimeField(null=True,blank=True)
    heure_fin = models.CharField(null=True,blank=True,max_length=30)
    prix = models.DecimalField(max_digits=10, decimal_places=2,null=True,blank=True,validators=[MinValueValidator(0)])
    don=models.BooleanField(null=True,blank=True)   #L'utilisateur peut choisir de donner sa place pour l'événement. Dans ce cas, le prix ne s'affiche pas.
    utilisateur = models.ForeignKey(Profil, on_delete=models.CASCADE)

    def __str__(self):
        return self.nom