from django.test import TestCase, Client
from django.urls import resolve
from evenements.views import formulaire, actu
from django.contrib.auth import login
from django.contrib.auth.models import User


class Evenements_page_test(TestCase):
    def test_actu_evenements(self):
        found = resolve('/evenements/')
        self.assertEqual(found.func, actu) 
    
    def test_evenements_formulaire(self):
        found = resolve('/evenements/formulaire/')
        self.assertEqual(found.func, formulaire)
    
    def test_evenements_page(self):
        response=self.client.get('/evenements/')
        message="nements"
        self.assertContains(response, message)