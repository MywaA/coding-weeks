from django.urls import re_path, path
from django.conf.urls import url
from .views import formulaire,actu,supprimer,modifier,search

urlpatterns = [
    path('formulaire/', formulaire, name='formulaire_evenements'),
    path('', actu, name='actu_evenements'),
    path('suppr_evenement/<int:profil_id>/<int:id_evenement>', supprimer , name='supprimer_evenement'),
    path('modif_evenement/<int:profil_id>/<int:id_evenement>', modifier , name='modifier_evenement'),
    path('search/',search,name='searcheve')
]