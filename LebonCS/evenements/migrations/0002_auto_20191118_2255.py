# Generated by Django 2.2.7 on 2019-11-18 21:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('evenements', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='evenements',
            name='prix',
            field=models.DecimalField(decimal_places=2, max_digits=10),
        ),
    ]
