# Generated by Django 2.2.7 on 2019-11-21 10:28

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('evenements', '0008_auto_20191119_2124'),
    ]

    operations = [
        migrations.AlterField(
            model_name='evenements',
            name='prix',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=10, null=True, validators=[django.core.validators.MinValueValidator(1)]),
        ),
    ]
