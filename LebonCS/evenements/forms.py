from django import forms
from .models import Evenements

#Formulaire pour mettre en ligne un événement

class EvenementsForm(forms.ModelForm):
    class Meta:
        model = Evenements
        fields = ('nom','date_debut','heure_debut','date_fin','heure_fin','description','prix','don')
        widgets = {'description': forms.Textarea,'don': forms.CheckboxInput,'date_debut': forms.SelectDateWidget,'date_fin': forms.SelectDateWidget}
        labels = {'nom':"Nom de l'évènement",'date_debut':"Date de début de l'évènement",'heure_debut':"Heure de début de l'évènement (faculatif)",'date_fin':"Date de fin de l'évènement (facultatif)",'heure_fin':"Heure de fin de l'évènement (facultatif)",'description':'Description (facultatif)','prix':'Prix (facultatif)','don':"Cochez si vous voulez donner"}