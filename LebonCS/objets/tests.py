from django.test import TestCase, Client
from django.urls import resolve
from objets.views import formulaire, actu
from django.contrib.auth import login
from django.contrib.auth.models import User


class Objet_page_test(TestCase):
    def test_actu_objet(self):
        found = resolve('/objets/')
        self.assertEqual(found.func, actu) 
    
    def test_objet_formulaire(self):
        found = resolve('/objets/formulaire/')
        self.assertEqual(found.func, formulaire)

    def test_objets_contenu(self):
        response=self.client.get('/objets/')
        message="Objets"
        self.assertContains(response, message)