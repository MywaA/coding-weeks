from django import forms
from .models import Objets


class ObjetsForm(forms.ModelForm):
    class Meta:
        model = Objets
        fields = ('nom','description','photo','prix','don')
        widgets = {'description': forms.Textarea,'don': forms.CheckboxInput}
        labels = {'nom':"Nom de l'objet",'description':'Description (facultatif)','photo':"Photo (facultatif)",'prix':'Prix (facultatif)','don':"Cochez si vous voulez donner l'objet"}