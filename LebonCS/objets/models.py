from django.db import models
from utilisateur.models import Profil
from django.core.validators import MinValueValidator
import django.utils

class Objets(models.Model):
    nom = models.CharField(max_length=200)
    description = models.CharField(max_length=5000,null=True,blank=True)
    photo = models.ImageField(upload_to='images_objets/',null=True,blank=True)
    prix = models.DecimalField(max_digits=10, decimal_places=2,null=True,blank=True,validators=[MinValueValidator(0)])
    don=models.BooleanField(null=True,blank=True) #L'utilisateur peut choisir de donner son objet. Dans ce cas, le prix ne s'affiche pas.
    date_post=models.DateTimeField(default=django.utils.timezone.now)
    utilisateur = models.ForeignKey(Profil, on_delete=models.CASCADE)

    def __str__(self):
        return self.nom