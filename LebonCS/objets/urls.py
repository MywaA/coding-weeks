from django.urls import re_path, path
from django.conf.urls import url
from .views import formulaire,actu,supprimer,modifier,search

urlpatterns = [
    path('formulaire/', formulaire, name='formulaire_objets'),
    path('', actu, name='actu_objets'),
    path('suppr_objet/<int:profil_id>/<int:id_objet>', supprimer , name='supprimer_objet'),
    path('modif_objet/<int:profil_id>/<int:id_objet>', modifier , name='modifier_objet'),
    path('search/',search,name='search3')
]