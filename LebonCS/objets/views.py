from django.shortcuts import render,redirect,HttpResponse
from .forms import ObjetsForm
from utilisateur.models import Profil
from cours.models import Cours
from covoiturage.models import Covoiturage
from evenements.models import Evenements
from .models import Objets
from django.contrib.auth.decorators import login_required

@login_required
def formulaire(request) :
    if request.method == 'POST': #on vérifie que le formulaire est rempli, sinon on en envoie un vide
        form = ObjetsForm(request.POST,request.FILES) #on vérifie que le formulaire est rempli, sinon on en envoie un vide
        if form.is_valid(): 
            objet = form.save(commit=False) #commit de nom,description,photo,prix,don
            objet.utilisateur_id = Profil.objects.get(user__id=request.user.id).id #lien ForeignKey avec Profil
            objet.save() #enregistrement d'une entrée (nom,description,photo,prix,don,utilisateur) dans la table Objets
            return redirect('/objets/')
    else : #envoi du formulaire vide
        form = ObjetsForm()

    return render(request, 'objets/formulaire_objet.html', locals())

@login_required
def modifier(request,profil_id,id_objet):
    objet=Objets.objects.get(id=id_objet) # on récupère l'objet que l'utilisateur veut modifier
    Objets.objects.filter(id=id_objet).delete()# on le supprime des annonces
    form = ObjetsForm(instance=objet)# on renvoie le formulaire prêt à être modifié
    return render(request, 'objets/formulaire_objet.html', locals())

def actu(request) :
    objets = Objets.objects.order_by('-date_post').all()
    return render(request,'objets/actu_objets.html',locals())

@login_required
def supprimer(request,profil_id,id_objet):

    Objets.objects.filter(id=id_objet).delete()
    objets=Objets.objects.filter(utilisateur__id=profil_id)
    evenements=Evenements.objects.filter(utilisateur__id=profil_id)
    cours=Cours.objects.filter(utilisateur__id=profil_id)
    covoiturages=Covoiturage.objects.filter(utilisateur__id=profil_id)

    return render(request,'utilisateur/page_perso.html',locals())

def search(request) :
    #récupération de la recherche
    nom=request.GET.get('searchForm4',None)

    #récupération objets correspondant à la recherche (des queryset)

    nomCorrespondant=set(Objets.objects.filter(nom__icontains=nom))
    descrCorrespondnat=set(Objets.objects.filter(description__icontains=nom))
    ObjetsCorrespondant= list(nomCorrespondant.union(descrCorrespondnat))
    return render(request,'objets/objetsearch.html',{'resultat':ObjetsCorrespondant})  