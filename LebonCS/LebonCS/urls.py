"""LebonCS URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
import cours.views,covoiturage.views,evenements.views,objets.views,utilisateur.views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', utilisateur.views.support, name = 'accueil'),
    path('register/', utilisateur.views.registration, name='registration'),
    path('connexion/', utilisateur.views.connexion, name = 'connexion'),
    path('deconnexion/', utilisateur.views.deconnexion, name = 'deconnexion'),
    path('cours/', include('cours.urls')),
    path('objets/', include('objets.urls')),
    path('evenements/', include('evenements.urls')),
    path('covoiturage/', include('covoiturage.urls')),
    path('page_contact/<int:id>',utilisateur.views.page_contact,name = 'page_contact'),
    path('objets/search/',objets.views.search,name='search'),
    path('page_perso/',utilisateur.views.page_perso,name = 'page_perso')
]



if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)