#Coding Weeks

Groupe n°9
Membres : Mattéo Faelli, Rudolf Worou, Alexandros Bampis, Pierre Artigala, Paul Blanchin, Alice Verwaerde
Nom du projet : LebonCS
Description du projet : Création d'un site web permettant d'échanger des cours, de vendre des objets, des places pour des événements et de proposer des covoiturages.